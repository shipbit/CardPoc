@tool
extends Node3D
@icon("./board.svg")

@export var tile_size:= 1.0
@export var tile_colors:= PackedColorArray()
@export var edge_count:= 6
@export var grid_size:= Vector2(4,4)

const board_tile = preload("./board_tile.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	generate_grid()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass;
	
func generate_grid():
	for x in range(grid_size.x):
		var pivot = Vector3.ZERO
		pivot.x = x * tile_size * cos(deg_to_rad(30))
		pivot.z = (x % 2) * tile_size / 2
		for y in range(grid_size.y):
			var color = tile_colors[get_child_count() % tile_colors.size()]
			var tile = board_tile.instantiate()
			tile.edge_count = edge_count
			tile.tile_size = tile_size
			tile.tile_color = color if color != null else Color.DARK_GOLDENROD 
			add_child(tile)
			tile.translate(pivot)
			print("(%s,%s) %s"% [x,y,pivot])
			pivot.z += tile_size

func update_tiles(property, value):
	for child in get_children():
		if not child.is_in_group("board_tile"):
			continue
		child.set_deferred(property,value)
