@tool
extends Node3D
class_name BoardTile

@icon("./tile.svg")

@export var tile_size:= 1.0
@export var tile_color:= Color.RED
@export var edge_count:= 6

@onready var mesh_instance:MeshInstance3D = $MeshInstance

# Called when the node enters the scene tree for the first time.
func _ready():
	updateMesh()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	updateMesh()

func updateMesh():
	if mesh_instance == null:
		return

	var diameter = tile_size / cos(deg_to_rad(30))

	mesh_instance.mesh.radial_segments = edge_count
	mesh_instance.mesh.top_radius = diameter / 2
	mesh_instance.mesh.bottom_radius = diameter / 2
	mesh_instance.mesh.material.albedo_color = tile_color
