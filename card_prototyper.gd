extends Node2D
class_name CardGame

enum TargetPlayer {
	Home,
	Guest,
	Both,
	None,
}

var game_state: Variant = {}
var turn_state: Variant = {}

@onready var turn_manager: TurnManager = $TurnManager
@onready var pause_menu: Menu = $PauseMenu
@onready var game_board: Board = $GameBoard

func _ready():
	turn_manager.turn_started.connect(turn_started)
	turn_manager.turn_completed.connect(turn_completed)
	turn_manager.turn_event.connect(turn_event)

	pause_menu.new_game_pressed.connect(start_game)
	pause_menu.reset_pressed.connect(reset_board)

	start_game()

func reset_board():
	game_board.reset_board()

func start_game():
	create_game_state()
	game_board.init(game_state)

	turn_manager.reset()
	turn_manager.process_turn()
	pause_menu.resume()

func turn_started(turn:int):
	print("turn %s started" % turn)
	pass

func turn_completed(turn:int):
	print("turn %s concluded" % turn)
	pass

func turn_event(event:TurnEvent):
	print("Executing %s" % event)
	event.execute(game_state,turn_state)

func create_game_state():
	game_state = {
		guest = {
			deck = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],
			draw =[1,2,3,4,5],
			graveyard = [7,8],
			discard = [13,14,15],
			hand = [9,10,11,12]
		},
		home = {
			deck = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],
			draw =[1,2,3,4,5],
			graveyard = [7,8],
			discard = [13,14,15],
			hand = [9,10,11,12]
		},
		board = [
			{ position=Vector2i(randi() % game_board.field_size.x, randi() % game_board.field_size.y), card=randi() % 15, owner= TargetPlayer.Home },
			{ position=Vector2i(randi() % game_board.field_size.x, randi() % game_board.field_size.y), card=randi() % 15, owner= TargetPlayer.Home },
			{ position=Vector2i(randi() % game_board.field_size.x, randi() % game_board.field_size.y), card=randi() % 15, owner= TargetPlayer.Home },
			{ position=Vector2i(randi() % game_board.field_size.x, randi() % game_board.field_size.y), card=randi() % 15, owner= TargetPlayer.Guest },
			{ position=Vector2i(randi() % game_board.field_size.x, randi() % game_board.field_size.y), card=randi() % 15, owner= TargetPlayer.Guest },
			{ position=Vector2i(randi() % game_board.field_size.x, randi() % game_board.field_size.y), card=randi() % 15, owner= TargetPlayer.Guest },
		]
	
	}

