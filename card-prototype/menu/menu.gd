extends ColorRect
class_name Menu

@onready var animator: AnimationPlayer = $AnimationPlayer
@onready var new_game_button: Button = %NewGame
@onready var resume_button: Button = %Resume
@onready var restart_button: Button = %Restart
@onready var quit_button: Button = %Quit

signal new_game_pressed
signal reset_pressed

func _ready():
	new_game_button.pressed.connect(new_game)
	resume_button.pressed.connect(resume)
	restart_button.pressed.connect(restart)
	quit_button.pressed.connect(get_tree().quit)
	size = get_viewport_rect().size

func _unhandled_input(event):
	if event.is_action_pressed("ui_cancel"):
		if get_tree().paused:
			resume()
		else:
			pause()

func resume():
	animator.play("Unpause")
	get_tree().paused = false
	Input.mouse_mode = Input.MOUSE_MODE_CONFINED

func pause():
	print("pause")
	animator.play("Pause")
	get_tree().paused = true
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE 

func new_game():
	new_game_pressed.emit();

func restart():
	reset_pressed.emit();
