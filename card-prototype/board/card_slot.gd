extends ColorRect
class_name CardSlot

var card: Variant
var board_position: Vector2i

@onready var CardTexture: TextureRect = $CardTexture

func _can_drop_data(at_position, data):
	return card == null

func _drop_data(at_position, data):
	card = data.card;
	CardTexture.texture = data.texture
	data.queue_free()
	
func clear():
	card = null
	CardTexture.texture = null
