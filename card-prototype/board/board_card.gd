extends TextureRect
class_name BoardCard

var card
var owned = false

func _get_drag_data(at_position):
	if not owned:
		return

	var preview = TextureRect.new()
	preview.texture = texture

	set_drag_preview(preview)

	return self
