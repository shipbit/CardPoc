extends Node2D
class_name Board

@export var background: Texture2D
@export var hand_size:= 5
@export var field_size:= Vector2i(3,2)

var card_slot_scene = preload("./card_slot.tscn")
var board_card_scene = preload("./board_card.tscn")

@onready var BackgroundTexture: TextureRect = %BackgroundTexture
@onready var FieldPool = %FieldPool
@onready var HandPool = %HandPool
@onready var EnemyPool = %EnemyPool

var card_slots: Dictionary = {}

# Called when the node enters the scene tree for the first time.
func _ready():
	set_process(Engine.is_editor_hint())

	BackgroundTexture.texture = background
	BackgroundTexture.size = get_viewport_rect().size
	FieldPool.columns = field_size.x

	reset_board()

func init(game_state):
	set_field()
	add_hand(game_state.home.hand)
	add_enemy(game_state.guest.hand)
	add_board(game_state.board)
	
func add_enemy(cards):
	clear(EnemyPool)

	for card in cards:
		var instance = board_card_scene.instantiate() as BoardCard
		instance.card = card
		EnemyPool.add_child(instance)

func add_hand(cards):
	clear(HandPool)

	for card in cards:
		var instance = board_card_scene.instantiate() as BoardCard
		instance.card = card
		instance.owned = true
		HandPool.add_child(instance)

func add_board(cards):
	for card in cards:
		var card_slot = card_slots[card.position]
		if card_slot != null:
			card_slot.card = card

func discard_hand():
	clear(HandPool)

func discard_field():
	for card_slot in FieldPool.get_children() as Array[CardSlot]:
		card_slot.clear()

func clear(node):
	for card in node.get_children():
		card.queue_free()


func reset_board():
	clear(HandPool)
	clear(EnemyPool)
	discard_field()

func set_field():
	clear(FieldPool)
	card_slots.clear()

	for y in range(field_size.y):
		for x in range(field_size.x):
			var card_slot = card_slot_scene.instantiate() as CardSlot
			card_slot.board_position = Vector2i(x,y)
			card_slots[card_slot.board_position] = card_slot
			FieldPool.add_child(card_slot)

