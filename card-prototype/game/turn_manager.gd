extends Node
class_name TurnManager

var current_turn = 1
var current_event = 0
var turn_order: Array[TurnEvent]

signal turn_event(event:TurnEvent, current_turn:int)
signal turn_completed(current_turn:int)
signal turn_started(current_turn:int)

func reset():
	current_turn = 1

func process_turn():
	current_event = 0
	turn_order = _read_turn_order(get_children())
	_start_event()

func complete_current_event():
	current_event += 1
	_start_event()	

func _read_turn_order(nodes: Array[Node]):
	var turn_events = []
	if nodes == null:
		return turn_events

	for node in nodes:
		if node is TurnEvent:
			turn_events.push_back(node)
		elif node is Selector:
			turn_events.append_array(_read_turn_order(node.get_selection(current_turn)))
		else:
			turn_events.append_array(_read_turn_order(node.get_children()))
	
	return turn_events

func _start_event():
	if  turn_order.size() <= current_event:
		_complete_turn()
		return

	var event = turn_order[current_event]
	event.concluded.connect(complete_current_event,CONNECT_ONE_SHOT)
	turn_event.emit(event)

func _complete_turn():
	turn_completed.emit(current_turn)
	current_turn += 1