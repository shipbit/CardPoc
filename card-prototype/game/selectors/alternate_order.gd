extends Selector

func get_selection(turn_count: int):
	var children = get_children()
	if turn_count % 2 == 0:
		children.reverse()
	return children