extends Node
class_name TurnEvent

signal concluded

func _ready():
	set_physics_process(false)
	set_process(false)

func execute(game_state, turn_state):
	print("Event execute not implemented %s" % self)
	conclude()
	
func conclude():
	concluded.emit()
