extends TurnEvent
@icon("./draw_card.png")

@export var target:= CardGame.TargetPlayer.Both
@export var amount:= 2