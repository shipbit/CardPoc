extends Node
class_name GameEvent

func _ready():
	set_physics_process(false)
	set_process(false)
